# iut-project

Application Node.js permettant à des utilisateurs d'être alertés des nouveaux films et de pouvoir gérer une liste de favoris.

L'application utilise un système de mail qui nécessite des variables d'environnements contenues dans le fichier .env à la racine du projet.
Après avoir créé un compte : https://ethereal.email/ . Vous devrez remplacer :
- le contenu de la variable `MAIL_USERNAME` par votre identifiant email donné
- le contenu de la variable `MAIL_PASSWORD` par le mot de passe donné

Pour configurer la base de données, il faudra également dans le fichier .env changer le contenu des variables d'environnement.

L'application nécessite différents packages à installer en lançant la commande `npm install`

`npm start` permet ensuite de lancer le serveur, et pour accéder aux différentes routes de l'application il faut se rendre sur http://localhost:3000/documentation 

