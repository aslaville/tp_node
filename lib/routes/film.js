'use strict';

const Joi = require('joi');

module.exports = [
    {
        // route permettant de créer un nouveau film
        method: 'post',
        path: '/film',
        options: {
            auth: {
                scope: ['admin']
            },
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    title: Joi.string().required().example('Pulp Fiction').description('Title of the movie'),
                    description: Joi.string().required().example('L\'odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent. Dans un restaurant, un couple de jeunes braqueurs, Pumpkin et Yolanda, discutent des risques que comporte leur activité').description('Description of the movie'),
                    release: Joi.string().required().example('1994-10-26').description('Release date of the movie'),
                    director: Joi.string().required().example('Quentin Tarantino').description('Director of the movie')
                })
            }
        },
        handler: async (request, h) => {

            const { filmService } = request.services();
            return await filmService.create(request.payload);
        }
    },
    {
        // route permettant de modifier les informations d'un film
        method: 'patch',
        path: '/film/{id}',
        options: {
            auth: {
                scope: ['admin']
            },
            tags: ['api'],
            validate: {
                params: Joi.object({
                    id: Joi.number().integer().greater(0).required().description('Id of the movie')
                }),
                payload: Joi.object({
                    title: Joi.string().required().example('Pulp Fiction').description('Title of the movie'),
                    description: Joi.string().required().example('L\'odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent. Dans un restaurant, un couple de jeunes braqueurs, Pumpkin et Yolanda, discutent des risques que comporte leur activité').description('Description of the movie'),
                    release: Joi.string().required().example('1994-10-26').description('Release date of the movie'),
                    director: Joi.string().required().example('Quentin Tarantino').description('Director of the movie')
                })
            }
        },
        handler: async (request, h) => {
            const { filmService } = request.services();
            return await filmService.patch(request.params.id,request.payload);
        }
    },
    {
        // route permettant d'ajouter un film à ses favoris
        method: 'get',
        path: '/film/favori/add/{id}',
        options: {
            auth: {
                scope: [ 'admin', 'user' ]
            },
            tags: ['api'],
            validate: {
                params: Joi.object({
                    id: Joi.number().integer().greater(0).required().description('Id of the movie')
                })
            }
        },
        handler: async (request, h) => {

            const { filmService } = request.services();
            return await filmService.addFavori(request.params.id, request.auth.credentials.id);

        }
    },
    {
        // route permettant de supprimer un film des favoris
        method: 'get',
        path: '/film/favori/delete/{id}',
        options: {
            auth: {
                scope: [ 'admin', 'user' ]
            },
            tags: ['api'],
            validate: {
                params: Joi.object({
                    id: Joi.number().integer().greater(0).required().description('Id of the movie')
                })
            }
        },
        handler: async (request, h) => {

            const { filmService } = request.services();
            return await filmService.deleteFavori(request.params.id, request.auth.credentials.id);

        }
    }

];