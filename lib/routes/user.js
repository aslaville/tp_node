'use strict';

const Joi = require('joi');

module.exports = [
    {
        // route permettant de créer un user
        method: 'post',
        path: '/user',
        options: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                    lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                    email: Joi.string().required().min(8).example('john.doe@mail.fr').description('Email of the user'),
                    username: Joi.string().required().min(3).example('johndoe').description('Username of the user'),
                    password: Joi.string().required().min(8).example('test1234').description('Password of the user')
                })
            }
        },
        handler: async (request, h) => {

            const { userService } = request.services();

            return await userService.create(request.payload);
        }
    },
    {
        // route permettant de récupérer les informations des users
        method: 'get',
        path: '/user',
        options: {
            auth: {
                scope: [ 'admin', 'user' ]
            },
            tags: ['api']
        },
        handler: async (request, h) => {

            const { userService } = request.services();
            return await userService.list();

        }
    },
    {
        // route permettant de supprimer un user
        method: 'delete',
        path: '/user/{id}',
        options: {
            auth: {
                scope: [ 'admin' ]
            },
            tags: ['api'],
            validate: {
                params: Joi.object({
                    id: Joi.string().required().example('1').description('ID of the user')
                })
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();

            return await userService.delete(request.params);
        }
    },
    {
        // route permettant de modifier un user
        method: 'patch',
        path: '/user/{id}',
        options: {
            auth: {
                scope: [ 'admin' ]
            },
            tags: ['api'],
            validate: {
                params: Joi.object({
                    id: Joi.string().required().example('1').description('ID of the user')
                }),
                payload: Joi.object({
                    firstName: Joi.string().required().example('Jack').description('Firstname of the user'),
                    lastName: Joi.string().required().example('Doe').description('Lastname of the user'),
                    username: Joi.string().required().example('johndoe').description('Username of the user')

                })
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();
            return await userService.patch(request.params.id,request.payload);
        }
    },
    {
        // route permettant de se connecter
        method: 'post',
        path: '/user/login',
        options: {
            auth: false,
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    email: Joi.string().required().min(8).example('john.doe@mail.fr').description('Email of the user'),
                    password: Joi.string().required().min(8).example('test1234').description('Password of the user')
                })
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();

            return await userService.login(request.payload);
        }
    },
    {
        // route permettant d'attribuer un user en tant qu'admin
        method: 'patch',
        path: '/user/admin/{id}',
        options: {
            auth: {
                scope: [ 'admin' ]
            },
            tags: ['api'],
            validate: {
                params: Joi.object({
                    id: Joi.string().required().example('1').description('ID of the user')
                }),
                payload: Joi.object({
                    role: Joi.string().required().example('admin').description('role of the user')
                })
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();
            return await userService.patch(request.params.id,request.payload);
        }
    }
];