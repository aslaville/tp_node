'use strict';

const  Boom  = require('@hapi/boom');

const { Service } = require('schmervice');
const Encrypt  = require('iut-encrypt-aslaville');
const Jwt = require('@hapi/jwt');


module.exports = class UserService extends Service {

    /** ajoute un nouvel utilisateur */
    create(user) {
        const { User } = this.server.models();
        // encrypt le mot de passe
        user.password = Encrypt.sha1(user.password);

        const { mailService } = this.server.services();

        // envoie un email de bienvenue
        mailService.sendWelcome(user);

        return User.query().insertAndFetch(user);


    }

    /** récupère tous les utilisateurs */
    list() {
        const { User } = this.server.models();

        return User.query();
    }

    /** méthode permettant de supprimer un utilisateur */
    async delete(user)
    {
        const { UserFilmFavori } = this.server.models();
        // récupère les favoris de l'utilisateur
        const favoriBD = await UserFilmFavori.query().select('*')
            .where('id_user', '=',user.id);

        // Si l'utilisateur possède des favoris
        if (favoriBD && favoriBD.length > 0) {
            // on les supprime
            favoriBD.forEach(async (fav) => {
                await UserFilmFavori.query().deleteById(fav.id);
            });
        }

        const { User } = this.server.models();
        return User.query().deleteById(user.id);
    }

    /** méthode permettant de modifier un utlisateur */
    patch(id, user)
    {
        const { User } = this.server.models();
        return User.query().patchAndFetchById(id, user);
    }

    /** méthode permettant de se connecter */
    async login(user) {
        const { User } = this.server.models();

        // récupère les informations de l'utilisateur par son adresse mail
        const userDB = await User.query().select('*').where('email', '=', user.email);
        // compare le mot de passe saisi avec celui en BD
        const comparaison = Encrypt.compareSha1(userDB[0].password, Encrypt.sha1(user.password));

        // Si les 2 mots de passe coincident
        if (comparaison) {
            // on renvoie un token
            const token = Jwt.token.generate(
                {
                    aud: 'urn:audience:iut',
                    iss: 'urn:issuer:iut',
                    id: userDB[0].id,
                    firstName: userDB[0].firstName,
                    lastName: userDB[0].lastName,
                    email: userDB[0].email,
                    scope: userDB[0].role

                },
                {
                    key: 'oxyoaprtjm', // La clé qui est définit dans lib/auth/strategies/jwt.js
                    algorithm: 'HS512'
                },
                {
                    ttlSec: 14400 // 4 hours
                }
            );
            return token;
        } else {
            // sinon une erreur
            return Boom.unauthorized('Unauthorized');
        }
    }

};