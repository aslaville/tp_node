'use strict';

const { Service } = require('schmervice');
const nodemailer = require('nodemailer');

require('dotenv').config();




module.exports = class MailService extends Service {

    async sendWelcome(user) {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        const testAccount = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD
            }
        });

        // send mail with defined transport object
        const info = await transporter.sendMail({
            from: '"Test Test" <foo@example.com>', // sender address
            to: user.email, // list of receivers
            subject: 'Bienvenue', // Subject line
            text: 'Bienvenue', // plain text body
            html: '<b>Bienvenue</b>' // html body
        });

        console.log('Message sent: %s', info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

    async notificationInsertionFilm(user, film) {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        const testAccount = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD
            }
        });

        // send mail with defined transport object
        const info = await transporter.sendMail({
            from: '"Test Test" <foo@example.com>', // sender address
            to: user.email, // list of receivers
            subject: 'Nouveau film inséré : ' + film.title, // Subject line
            text: 'Nouveau film inséré : ' + + film.title, // plain text body
            html: '<b>Nouveau film inséré</b>' // html body
        });

        console.log('Message sent: %s', info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

    async notificationFavori(user, film) {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        const testAccount = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD
            }
        });

        // send mail with defined transport object
        const info = await transporter.sendMail({
            from: '"Test Test" <foo@example.com>', // sender address
            to: user.email, // list of receivers
            subject: film.title + ' a été modifié', // Subject line
            text: film.title + ' a été modifié', // plain text body
            html: '<b>Un film de vos favoris a été modifié</b>' // html body
        });

        console.log('Message sent: %s', info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }
}