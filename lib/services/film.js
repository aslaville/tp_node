'use strict';
const  Boom  = require('@hapi/boom');
const { Service } = require('schmervice');



module.exports = class FilmService extends Service {

    /** méthode permettant de créer un nouveau film */
    async create(film) {
        const { Film } = this.server.models();
        const { User } = this.server.models();

        // récupère les utilisateurs en BD
        const usersBD = await User.query().select('*');

        const { mailService } = this.server.services();

        // envoie un mail pour chaque user
        usersBD.forEach((user) => {
            mailService.notificationInsertionFilm(user, film);
        });
        
        return Film.query().insertAndFetch(film);
    }

    /** méthode permettant de modifier un film */
    async patch(id, film)
    {
        const { UserFilmFavori } = this.server.models();
        const { User } = this.server.models();

        // récupère les données de la table favoris par rapport à l'id du film
        const usersFavorisBD = await UserFilmFavori.query().select('*')
            .where('id_film', '=', id);

        // récupère les id des users ayant le film en favoris
        const idUsers = usersFavorisBD.map((user) => user.id_user);

        // récupère les informations des users précédents.
        const usersBD = await User.query().select('*').whereIn('id', idUsers);

        const { mailService } = this.server.services();
        // envoie un mail pour chaque user
        usersBD.forEach((user) => {
            mailService.notificationFavori(user, film);
        });

        const { Film } = this.server.models();
        return Film.query().patchAndFetchById(id, film);
    }


    /** ajoute un film aux favoris de l'utilisateur */
    async addFavori(idFilm, idUser)
    {
        const { UserFilmFavori } = this.server.models();

        // récupère les favoris de l'utilisateur par rapport au film.
        const favoriBD = await UserFilmFavori.query().select('*')
            .where('id_film', '=',idFilm)
            .andWhere('id_user', '=', idUser);

        // Si le favori n'existe pas déjà pour ce film
        if (favoriBD && favoriBD.length === 0) {
            // on ajoute le film aux favoris
            const favori = {
                'id_user': idUser,
                'id_film': idFilm
            };
            return UserFilmFavori.query().insertAndFetch(favori);
        } else {
            // sinon on renvoie un message d'erreur
            return Boom.notAcceptable('Le film est déjà dans vos favoris');
        }



    }

    /** supprime un film des favoris */
    async deleteFavori(idFilm, idUser)
    {
        const { UserFilmFavori } = this.server.models();

        // récupère les favoris de l'utilisateur par rapport au film.
        const favoriBD = await UserFilmFavori.query().select('*')
            .where('id_film', '=',idFilm)
            .andWhere('id_user', '=', idUser);

        // si le film est bien présent dans les favoris
        if (favoriBD && favoriBD.length > 0) {
            // on le retire
            return UserFilmFavori.query().deleteById(favoriBD[0].id);
        } else {
            // sinon on envoie un message d'erreur
            return Boom.notAcceptable('Le film n\'existe pas dans vos favoris');
        }



    }
};