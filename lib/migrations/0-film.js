'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('film', (table) => {

            table.increments('id').primary();
            table.string('title').notNull();
            table.string('description');
            table.string('director').notNull();
        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('film');
    }
};
