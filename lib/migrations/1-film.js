'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.alterTable('film', (table) => {

            table.date('release').notNull();
            table.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
            table.dateTime('updatedAt').notNull().defaultTo(knex.fn.now());
        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('film');
    }
};
