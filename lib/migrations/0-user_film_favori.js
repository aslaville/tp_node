'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('user_film_favori', (table) => {

            table.increments('id').primary();
            table.integer('id_user').notNull();
            table.integer('id_film').notNull();

            table.foreign('id_user').references('id').inTable('user');
            table.foreign('id_film').references('id').inTable('films');


        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('user_film_favori');
    }
};
