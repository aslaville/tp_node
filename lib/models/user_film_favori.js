'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class UserFilmFavori extends Model {

    static get tableName() {

        return 'user_film_favori';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            id_user: Joi.number().example(1).description('ID of the user'),
            id_film: Joi.number().example(1).description('ID of the movie')

        });
    }

};