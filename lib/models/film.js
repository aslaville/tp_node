'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class Film extends Model {

    static get tableName() {

        return 'film';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            title: Joi.string().example('Pulp Fiction').description('Title of the movie'),
            description: Joi.string().example('L\'odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent. Dans un restaurant, un couple de jeunes braqueurs, Pumpkin et Yolanda, discutent des risques que comporte leur activité. Deux truands, Jules Winnfield et son ami Vincent Vega, qui revient d\'Amsterdam, ont pour mission de récupérer une mallette au contenu mystérieux et de la rapporter à Marsellus Wallace.').description('Description of the movie'),
            release: Joi.date().example('1994-10-26').description('Release date of the movie'),
            director: Joi.string().example('Quentin Tarantino').description('Director of the movie'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()

        });
    }

    $beforeInsert(queryContext) {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};