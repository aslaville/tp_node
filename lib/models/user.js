'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class User extends Model {

    static get tableName() {

        return 'user';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            firstName: Joi.string().min(3).example('John').description('Firstname of the user'),
            lastName: Joi.string().min(3).example('Doe').description('Lastname of the user'),
            email: Joi.string().min(8).example('john.doe@email.fr').description('email of the user'),
            password: Joi.string().min(8).example('test1234').description('Password of the user'),
            username: Joi.string().min(5).example('johndoe').description('username of the user'),
            createdAt: Joi.date(),
            updatedAt: Joi.date(),
            role: Joi.string().example('user').description('role of the user')

        });
    }

    $beforeInsert(queryContext) {
        this.role = 'user';
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};